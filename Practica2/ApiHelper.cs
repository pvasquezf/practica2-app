﻿using Newtonsoft.Json;
using Practica2.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Practica2
{
    public class ApiHelper
    {
        public static async Task<Usuario> getLogin(string usuario, string pass)
        {
            Usuario result = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://practica2-api.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var content = new UsuarioOut() { User_name = usuario, Password = pass };
                var json = JsonConvert.SerializeObject(content);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("api/login", stringContent);
                if (response.IsSuccessStatusCode)
                {
                    var a = await response.Content.ReadAsStringAsync();
                    Debug.WriteLine(a);
                    a = a.Replace("\\", "").Trim(new char[1] { '"' });
                    result = new Usuario() { id = Convert.ToInt32(a) };
                }
                return result;
            }
        }
    }
}