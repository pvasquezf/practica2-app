﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="Practica2.Registro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Registro de Usuario</h1>
    <hr>

        <h1><strong>CREAR USUARIO</strong></h1>
    <table class="nav-justified">
        <tr>
            <td>ID_Usuario</td>
            <td>
                <asp:TextBox ID="tb_idusuario" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>UserName</td>
            <td>
                <asp:TextBox ID="tb_username" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Correo</td>
            <td>
                <asp:TextBox ID="tb_correo" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Password</td>
            <td>
                <asp:TextBox ID="tb_password" runat="server"></asp:TextBox>
            </td>
        </tr>

        
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="crear" runat="server" Text="Crear Usuario" OnClick="crear_Click" />
            </td>
        </tr>
    </table>

</asp:Content>
