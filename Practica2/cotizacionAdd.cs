﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Practica2.Model;
using Practica2.ModelOut;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace Practica2
{
    public class cotizacionAdd
    {
        public static HttpClient ApiClient { get; set; }

        //producto
        public static ArrayList nombre = new ArrayList();
        public static ArrayList idProductos = new ArrayList();

        //cotizacion
        public static ArrayList noCotizacion = new ArrayList();

        //productoCotizacion
        //public static ArrayList subTotalpc;

        public static void CrearUsuario()
        {
            var url = "https://practica2-api.azurewebsites.net/api/producto";
            WebClient wc = new WebClient();
            var datos = wc.DownloadString(url);
            var rs = JsonConvert.DeserializeObject<productoData[]>(datos);
            foreach (var producto in rs) {
                nombre.Add(producto.Nombre);
                idProductos.Add(producto.Id_producto);
            }
        }

        public static void cotizaciones(String id)
        {
            var url = "https://practica2-api.azurewebsites.net/api/cotizacion/"+id;
            WebClient wc = new WebClient();
            var datos = wc.DownloadString(url);
            var rs = JsonConvert.DeserializeObject<cotizacionData[]>(datos);
            foreach (var cotizacion in rs)
            {
                noCotizacion.Add(cotizacion.Id_cotizacion);
            }
        }

        public static async Task<bool> addProductoCotizacion(int cantidad, String producto, int idCotizacion)
        {
            int idProducto = -1;
            for (int i = 0; i < nombre.Count; i++)
            {
                if (producto.Equals(nombre[i]))
                {
                    idProducto = (Int32)idProductos[i];
                    break;
                }
            }
            System.Diagnostics.Debug.WriteLine(idProducto+"");
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://practica2-api.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var content = new productoCotizacionOut()
                {
                    Id_cotizacion = idCotizacion,
                    Id_producto = idProducto,
                    Cantidad = cantidad
                };
                var json = JsonConvert.SerializeObject(content);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("api/cotizacionproducto", stringContent);
                if (response.IsSuccessStatusCode)
                {
                    var a = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine(a+"*");
                    return true;
                }
                System.Diagnostics.Debug.WriteLine("*");
                return false;
            }
        }

        
    }

    public class productoData
    {
        public int Id_producto { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public double Precio_base { get; set; }
    }

    public class cotizacionData
    {
        public int Id_cotizacion { get; set; }
        public int Id_cuenta { get; set; }
        public string Fecha_cotizacion { get; set; }
        public double Precio_total { get; set; }
    }

    public class cotizacionProducto
    {
        public int Id_CP { get; set; }
        public int Id_cotizacion { get; set; }
        public int Id_producto { get; set; }
        public int Cantidad { get; set; }
        public double Subtotal { get; set; }
    }
}