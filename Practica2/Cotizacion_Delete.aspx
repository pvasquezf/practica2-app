﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cotizacion_Delete.aspx.cs" Inherits="Practica2.Cotizacion_Delete" Async="true"%>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<h1>Cotizacion</h1>
<hr>

<div class="home_content text-center">
<h2>Eliminar productos</h2>
</div>

<div class="row tex">
    <div class="grid-container">
        <div class="Crear Usuario">
            <div class="log">
                <form asp-action="Create">
                    <div class="form-group">
                        <label class="control-label">Ingrese su No. Cotizacion</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox runat="server" ID="txtCuenta"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnCuenta" runat="server" Text="Ingresar" OnClick="cargarCotizacion" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Seleccione id. Producto a eliminar</label>
                    </div>
                    <div class="form-group">
                        <select runat="server" name="rol" ID ="ddCotizacicon">
                        </select>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnAdd" runat="server" Text="Eliminar" OnClick="eliminarProducto"/>
                    </div>
                    <div class="form-group">
                        <asp:label class="control-label" ID="lblmessage" runat="server" value=""></asp:label>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</asp:Content>