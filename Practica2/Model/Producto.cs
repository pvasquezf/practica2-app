﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Practica2.Model
{
    public class Producto
    {
        int id;
        string nombre;
        string descripcion;
        string precio_base;

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public string Precio_base { get => precio_base; set => precio_base = value; }
    }
}