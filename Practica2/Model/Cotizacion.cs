﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Practica2.Model
{
    public class Cotizacion
    {
        public int Id_cotizacion { get; set; }
        public int Id_cuenta { get; set; }
        public String Fecha_cotizacion { get; set; }
        public double Precio_total { get; set; }
    }
}