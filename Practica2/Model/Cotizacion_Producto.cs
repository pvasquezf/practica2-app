﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Practica2.Model
{
    public class Cotizacion_Producto
    {
        public Cotizacion_Producto(int id_CP, int id_cotizacion, int id_producto, int id_cantidad, float subtotal)
        {
            this.Id_CP = id_CP;
            this.Id_cotizacion = id_cotizacion;
            this.Id_producto = id_producto;
            this.Cantidad = id_cantidad;
            this.Subtotal = subtotal;
        }

        public int Id_CP { get; set; }
        public int Id_cotizacion { get; set; }
        public int Id_producto { get; set; }
        public int Cantidad { get; set; }
        public float Subtotal { get; set; }
    }
}