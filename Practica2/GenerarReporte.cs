﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Practica2.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Practica2
{
    public class GenerarReporte
    {
        public static async Task<bool> Generar(int id_cotizacion)
        {
            double total = 0.0;

            string url = "https://practica2-api.azurewebsites.net/api/cotizacionproducto/" + id_cotizacion;
            bool generado = false;
            List<Cotizacion_Producto> lst_cp = new List<Cotizacion_Producto>();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    lst_cp = JsonConvert.DeserializeObject<List<Cotizacion_Producto>>(reader.ReadToEnd());
                }
                if (lst_cp.Count != 0)
                {
                    Document doc = new Document(PageSize.LETTER);
                    PdfWriter writer = PdfWriter.GetInstance(doc,
                            new FileStream(@"cotizacion.pdf", FileMode.Create));
                    doc.AddTitle("Cotizacion de Productos");
                    doc.AddCreator("Andree Avalos");
                    doc.Open();
                    iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                    doc.Add(new Paragraph("Cotizacion de Productos"));
                    doc.Add(Chunk.NEWLINE);

                    PdfPTable tblPrueba = new PdfPTable(3);
                    tblPrueba.WidthPercentage = 100;

                    PdfPCell producto = new PdfPCell(new Phrase("Producto", _standardFont));
                    producto.BorderWidth = 0;
                    producto.BorderWidthBottom = 0.75f;

                    PdfPCell cantidad = new PdfPCell(new Phrase("Cantidad", _standardFont));
                    cantidad.BorderWidth = 0;
                    cantidad.BorderWidthBottom = 0.75f;

                    PdfPCell Subtotal = new PdfPCell(new Phrase("Subtotal", _standardFont));
                    Subtotal.BorderWidth = 0;
                    Subtotal.BorderWidthBottom = 0.75f;

                    tblPrueba.AddCell(producto);
                    tblPrueba.AddCell(cantidad);
                    tblPrueba.AddCell(Subtotal);

                    foreach (Cotizacion_Producto item in lst_cp)
                    {
                        producto = new PdfPCell(new Phrase(item.Id_CP + "", _standardFont));
                        producto.BorderWidth = 0;

                        cantidad = new PdfPCell(new Phrase("" + item.Cantidad, _standardFont));
                        cantidad.BorderWidth = 0;

                        Subtotal = new PdfPCell(new Phrase("" + item.Subtotal, _standardFont));
                        Subtotal.BorderWidth = 0;

                        tblPrueba.AddCell(producto);
                        tblPrueba.AddCell(cantidad);
                        tblPrueba.AddCell(Subtotal);

                        total += item.Subtotal;
                    }

                    producto = new PdfPCell(new Phrase("", _standardFont));
                    producto.BorderWidth = 0;

                    cantidad = new PdfPCell(new Phrase("TOTAL", _standardFont));
                    cantidad.BorderWidth = 0;

                    Subtotal = new PdfPCell(new Phrase("" + total, _standardFont));
                    Subtotal.BorderWidth = 0;

                    tblPrueba.AddCell(producto);
                    tblPrueba.AddCell(cantidad);
                    tblPrueba.AddCell(Subtotal);

                    doc.Add(tblPrueba);

                    doc.Close();
                    writer.Close();

                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri("https://practica2-api.azurewebsites.net/");
                    Cotizacion update = new Cotizacion();
                    update.Id_cotizacion = id_cotizacion;
                    update.Id_cuenta = 0;
                    update.Fecha_cotizacion = "";
                    update.Precio_total = total;
                    HttpResponseMessage responsew = await client.PutAsJsonAsync(
                        $"api/cotizacion//{id_cotizacion}", update);
                    if (responsew.IsSuccessStatusCode)
                    {
                        generado = true;
                    }
                    else generado = false;
                }
            }
            catch (Exception)
            {
                generado = false;
            }
            return generado;
        }

    }
}