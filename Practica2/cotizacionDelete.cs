﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Practica2
{
    public class cotizacionDelete
    {

        public static ArrayList noCotizacion;

        public static void cotizaciones(String id)
        {
            noCotizacion = new ArrayList();
            var url = "https://practica2-api.azurewebsites.net/api/cotizacionproducto/" + id;
            WebClient wc = new WebClient();
            var datos = wc.DownloadString(url);
            var rs = JsonConvert.DeserializeObject<cotiproducto[]>(datos);
            foreach (var cotizacion in rs)
            {
                noCotizacion.Add(cotizacion.Id_CP);
            }
        }



        public static async Task<bool> deletepc(string idcp)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://practica2-api.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.DeleteAsync("api/cotizacionproducto/"+idcp);
                if (response.IsSuccessStatusCode)
                {
                    var a = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine(a + "*");
                    return true;
                }
                System.Diagnostics.Debug.WriteLine("*");
                return false;
            }
        }



    }



    public class cotiproducto
    {
        public int Id_CP { get; set; }
        public int Id_cotizacion { get; set; }
        public int Id_producto { get; set; }
        public int Cantidad { get; set; }
        public double Subtotal { get; set; }
    }
}