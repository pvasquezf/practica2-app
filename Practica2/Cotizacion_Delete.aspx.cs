﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practica2
{
    public partial class Cotizacion_Delete : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void cargarCotizacion(object sender, EventArgs e)
        {
            String aux = txtCuenta.Text;
            cotizacionDelete.cotizaciones(aux);
            foreach (object item in cotizacionDelete.noCotizacion)
            {
                ddCotizacicon.Items.Add(new ListItem(item.ToString(), item.ToString()));
            }
        }

        public async void eliminarProducto(object sender, EventArgs e)
        {
            String idcp = ddCotizacicon.Value;
            System.Diagnostics.Debug.WriteLine(idcp);
            bool productoCoti = await cotizacionDelete.deletepc(idcp);

            if (productoCoti)
            {
                lblmessage.Text = "Producto eliminado";
            }
            else
            {
                lblmessage.Text = "El producto no se elimino";
            }
        }
    }
}