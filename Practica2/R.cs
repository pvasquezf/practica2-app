﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;

namespace Practica2
{
    public class R
    {
        public static string CrearUsuario(int id, String username, String correo, String password)
        {
            String serviceurl = string.Format("https://practica2-api.azurewebsites.net/api/usuario");
            CrearUsuarioJSON usuarioJSON = new CrearUsuarioJSON
            {
                Id_usuario = id,
                User_name = username,
                Correo = correo,
                Pass = password
            };

            HttpWebRequest request = WebRequest.Create(serviceurl) as HttpWebRequest;
            //Configurar las propiedad del objeto de llamada
            request.Method = "POST";
            request.ContentType = "application/json";

            string sb = JsonConvert.SerializeObject(usuarioJSON);

            System.Diagnostics.Debug.WriteLine(sb);

            //Convertir el objeto serializado a arreglo de byte
            Byte[] bt = Encoding.UTF8.GetBytes(sb);

            Stream st = request.GetRequestStream();
            st.Write(bt, 0, bt.Length);
            st.Close();
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                //Leer el resultado de la llamada
                Stream stream1 = response.GetResponseStream();
                StreamReader sr = new StreamReader(stream1);
                string strsb = sr.ReadToEnd();
                System.Diagnostics.Debug.WriteLine(strsb);

                return strsb;
            }

        }

        public static string CrearCuenta(int idcuenta, string fecha, int estado, int idusuario)
        {
            String serviceurl = string.Format("https://practica2-api.azurewebsites.net/api/cuenta");
            CrearCuentaJSON cuentaJSON = new CrearCuentaJSON
            {
                Id_cuenta = idcuenta,
                Fecha_creacion = fecha,
                Estado = estado,
                Id_usuario = idusuario

            };

            HttpWebRequest request = WebRequest.Create(serviceurl) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/json";

            string sb = JsonConvert.SerializeObject(cuentaJSON);

            System.Diagnostics.Debug.WriteLine(sb);

            Byte[] bt = Encoding.UTF8.GetBytes(sb);


            Stream st = request.GetRequestStream();
            st.Write(bt, 0, bt.Length);
            st.Close();
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                Stream stream1 = response.GetResponseStream();
                StreamReader sr = new StreamReader(stream1);
                string strsb = sr.ReadToEnd();
                return strsb;
            }

        }

        public static string CrearCotizacion(int idcuenta, string fecha, double precio, int idcotizacion)
        {
            String serviceurl = string.Format("https://practica2-api.azurewebsites.net/api/cotizacion");
            CrearCotizacionJSON CotizacionJSON = new CrearCotizacionJSON
            {
                Id_cuenta = idcuenta,
                Fecha_cotizacion = fecha,
                Precio_total = precio,
                Id_cotizacion = idcotizacion

            };

            HttpWebRequest request = WebRequest.Create(serviceurl) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/json";

            string sb = JsonConvert.SerializeObject(CotizacionJSON);
            Byte[] bt = Encoding.UTF8.GetBytes(sb);
            Stream st = request.GetRequestStream();
            st.Write(bt, 0, bt.Length);
            st.Close();
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                //Leer el resultado de la llamada
                Stream stream1 = response.GetResponseStream();
                StreamReader sr = new StreamReader(stream1);
                string strsb = sr.ReadToEnd();
                return strsb;
            }
        }
    }

    public class CrearUsuarioJSON
    {
        public int Id_usuario { get; set; }
        public string User_name { get; set; }
        public string Correo { get; set; }
        public string Pass { get; set; }

    }

    public class CrearCuentaJSON
    {
        public int Id_cuenta { get; set; }
        public string Fecha_creacion { get; set; }
        public int Estado { get; set; }
        public int Id_usuario { get; set; }
    }

    public class CrearCotizacionJSON
    {
        public int Id_cotizacion { get; set; }
        public int Id_cuenta { get; set; }
        public string Fecha_cotizacion { get; set; }
        public double Precio_total { get; set; }
    }
}