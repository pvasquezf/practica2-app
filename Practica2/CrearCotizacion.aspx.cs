﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practica2
{
    public partial class CrearCotizacion : System.Web.UI.Page
    {
        private int idcuenta = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            idcuenta = Int32.Parse(Session["idcuenta"].ToString());
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string fecha = "";
            double precio = 0.0;
            int id_cotizacion = -1;

            string idcotizacion = Practica2.R.CrearCotizacion(idcuenta, fecha, precio, id_cotizacion);
            Session["idcotizacion"] = idcotizacion;
        }
    }
}