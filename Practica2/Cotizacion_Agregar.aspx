﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cotizacion_Agregar.aspx.cs" Inherits="Practica2.Cotizacion_Agregar" Async="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <h1>Cotizacion</h1>
    <hr>

    <div class="home_content text-center">
    <h2>Agregar productos</h2>
    </div>

<div class="row tex">
    <div class="grid-container">
        <div class="Crear Usuario">
            <div class="log">
                <form asp-action="Create">
                    <div class="form-group">
                        <label class="control-label">Ingrese su No. Cuenta</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox runat="server" ID="txtCuenta"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnCuenta" runat="server" Text="Ingresar" OnClick="cargarCotizacion"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Seleccione No. Cotizacion</label>
                    </div>
                    <div class="form-group">
                        <select runat="server" name="rol" ID ="ddCotizacicon">
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Seleccionar producto</label>
                    </div>
                    <div class="form-group">
                        <select runat="server" name="rol" ID ="ddProducto">
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Cantidad</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox runat="server" ID="txtCantidad"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnAdd" runat="server" Text="Agregar" OnClick="agregarProducto"/>
                        <asp:Button ID="btnImprimir" runat="server" Text="Imprimir Cotizacion" />
                    </div>
                    <div class="form-group">
                        <asp:label class="control-label" ID="lblmessage" runat="server" value=""></asp:label>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</asp:Content>