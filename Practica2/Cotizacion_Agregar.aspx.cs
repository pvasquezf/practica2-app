﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practica2
{
    public partial class Cotizacion_Agregar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void cargarCotizacion(object sender, EventArgs e) {
            String aux = txtCuenta.Text;
            cotizacionAdd.cotizaciones(aux);
            foreach (object item in cotizacionAdd.noCotizacion)
            {
                ddCotizacicon.Items.Add(new ListItem(item.ToString(), item.ToString()));
            }
            cotizacionAdd.CrearUsuario();
            foreach (object item in cotizacionAdd.nombre)
            {
                ddProducto.Items.Add(new ListItem(item.ToString(), item.ToString()));
            }
        }

        public async void agregarProducto(object sender, EventArgs e) {
            String producto = ddProducto.Value;
            int cantidad = 0;
            Int32.TryParse(txtCantidad.Text, out cantidad);
            int idCotizacion = 0;
            Int32.TryParse(ddCotizacicon.Value, out idCotizacion);

            bool productoCoti = await cotizacionAdd.addProductoCotizacion(cantidad, producto, idCotizacion);

            if (productoCoti)
            {
                lblmessage.Text = "Producto agregado";
            }
            else
            {
                lblmessage.Text = "El producto no se Agrego";
            }
        }

        public void imprimir() {

        }

    }
}