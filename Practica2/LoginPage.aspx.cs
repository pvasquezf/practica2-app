﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Practica2.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practica2
{
    public partial class LoginPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["idusuario"] != null)
            {
                Response.Redirect("Default.aspx", false);
            }
        }

        protected async void IniciarSesion(object sender, EventArgs e)
        {
            Usuario user = await ApiHelper.getLogin(nombreUsuario.Text, password.Text);
            if (user != null && user.id != -1)
            {
                resultado.Text = "VALIDO";
                Session["idusuario"] = user.id;
                Session["nombreusuario"] = nombreUsuario.Text;
                Response.Redirect("Default.aspx", false);
            }
            else
            {
                resultado.Text = "INVALIDO";
            }
        }

        //public async Task getLogin(string usuario, string pass)
        //{
        //    using(var client = new HttpClient())
        //    {
        //        //client.BaseAddress = new Uri("https://practica2-api.azurewebsites.net/");
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        //        //HttpResponseMessage response = await client.GetAsync("https://practica2-api.azurewebsites.net/api/cotizacion/1");
        //        //HttpResponseMessage response = await client.GetAsync("https://xkcd.com/info.0.json");
        //        HttpResponseMessage response = await client.GetAsync("http://my-json-server.typicode.com/pvasquezf/demo/user");
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var a = await response.Content.ReadAsStringAsync();
        //            a = a.Replace("\\", "").Trim(new char[1] { '"' });
        //            var b = JsonConvert.DeserializeObject<IEnumerable<Usuario>>(a);
        //        }
        //    }
        //}

    }
}