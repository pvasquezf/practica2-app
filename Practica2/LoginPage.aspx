﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="Practica2.LoginPage" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="Content/bootstrap.css"/>
    <link rel="stylesheet" href="Content/main.css"/>
    <title>Login</title>
</head>
<body>
    <form id="form1" runat="server">
        <center>
            <h1>Bienvenido</h1>
        </center>
        <div class="form">
            <form action="/Login" method="get">
                <center>
                    <h1>Iniciar Sesión</h1>
                </center>
                <br/>
                <center>
                    <asp:TextBox runat="server" ID="nombreUsuario" ></asp:TextBox>
                    <br/>
                    <br/>
                    <asp:TextBox runat="server" ID="password" TextMode="Password"></asp:TextBox>
                    <br/>
                    <br/>
                    <asp:Button runat="server" OnClick="IniciarSesion" Text = "Iniciar Sesión"/>
                </center/>
                <asp:Label runat="server" ID="resultado"></asp:Label>
            </form>
        </div>
        <script src="Scripts/bootstrap.js"></script>
    </form>
</body>
</html>
