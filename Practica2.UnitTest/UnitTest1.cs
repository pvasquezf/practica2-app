using NUnit.Framework;
using Practica2;
using Practica2.Model;
using System.Threading.Tasks;



namespace Tests
{

    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task TestLogin()
        {
            Usuario a = await ApiHelper.getLogin("AndreeAvalos", "1234");
            Assert.IsNotNull(a);
            Assert.AreNotEqual(a, -1);
        }

        [Test]
        public void TestRegistrar()
        {
            string esperado = "true";
            int Id_usuario = 10;
            string User_name = "username";
            string Correo = "correo";
            string Pass = "password";
            string actual = R.CrearUsuario(Id_usuario, User_name, Correo, Pass);
            Assert.AreEqual(esperado, actual);
        }

        [Test]
        public async Task TestProductoCotizacionAdd()
        {
            bool productoCoti = await cotizacionAdd.addProductoCotizacion(3, "PEPSI", 1);
            Assert.IsTrue(productoCoti);
        }

        [Test]
        public async Task TestGenerarCotizacion()
        {
            bool generado = await GenerarReporte.Generar(1);
            Assert.IsTrue(generado);
        }

        [Test]
        public void TestCrearCuenta()
        {
            string noesperado = "-1";
            int id_cuenta = -1;
            string fecha = "";
            int estado = 1;
            int id_usuario = 1;
            string actual = Practica2.R.CrearCuenta(id_cuenta, fecha, estado, id_usuario);
            Assert.AreNotEqual(noesperado, actual);
        }

        [Test]
        public void TestCrearCotizacion()
        {
            string noesperado = "-1";
            int id_cuenta = 1;
            string fecha = "";
            double precio = 0.0;
            int id_cotizacion = -1;
            string actual = R.CrearCotizacion(id_cuenta, fecha, precio, id_cotizacion);
            Assert.AreNotEqual(noesperado, actual);
        }

        [Test]
        public async Task TestPCDelete()
        {
            bool productoCoti = await cotizacionDelete.deletepc("4");
            Assert.IsTrue(productoCoti);
        }

    }
}